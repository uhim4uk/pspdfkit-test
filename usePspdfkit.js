import PSPDFKit from 'pspdfkit';

const { Rect } = PSPDFKit.Geometry;
const { RectangleAnnotation, TextAnnotation } = PSPDFKit.Annotations;

const SYMBOL_LENGTH = 8;
const PADDING = 4;
const STROKE_WIDTH = 2;
const BOTTOM_POSITION = 346;
const DRAWING_BORDER_WIDTH = 29;
const ROW_HEIGHT = 22;
const MIN_ROW_WIDTH = 16;
const FONT_SIZE = 12;
const LICENSE_KEY = import.meta.env.VITE_PSPDFKIT_LICENSE_KEY;

function unloadPspdfkit(pspdfkitInstance) {
  return pspdfkitInstance ? PSPDFKit.unload(pspdfkitInstance) : true;
}

/**
 * @param {Instance} instance
 */
function removeNotUsedMenuItems(instance) {
  const typesToRemove = [
    'sidebar-thumbnails',
    'sidebar-document-outline',
    'sidebar-annotations',
    'sidebar-bookmarks',
    'pager',
    'search',
    'document-editor',
    'document-crop'
  ];
  instance.setToolbarItems(
    instance.toolbarItems.filter(({ type }) => !typesToRemove.includes(type))
  );
}

/**
 * @param {String} selector
 * @param {String} pdfPath
 */
async function loadPspdfkit(selector, pdfPath) {

  const config = {
    baseUrl: `${window.location.protocol}//${window.location.host}/assets/pspdfkit/`,
    document: pdfPath,
    autoSaveMode: PSPDFKit.AutoSaveMode.DISABLED
  };

  if (selector) {
    config.container = selector;
    config.isEditableAnnotation = annotation => {
      return !annotation?.customData?.disableEditing;
    };
  }
  // init in headless mode if there is no css selector
  if (!selector) {
    config.headless = true;
  }

  return PSPDFKit.load(config)
    .then(instance => {
      if (selector) removeNotUsedMenuItems(instance);
      return instance;
    })
    .catch(error => {
      console.error(error);
    });
}

/**
 * @param {String | Number | {value: String | Number}} cell
 */
function getCellValue(cell) {
  let value = '';
  if (typeof cell === 'number' || typeof cell === 'string') {
    value = cell;
  } else if (cell?.value !== undefined) {
    value = cell.value;
  }

  return `${value}`;
}

/**
 * @param {Array<Array>} tableRows
 */
function calculateColumnsLength(tableRows) {
  const result = [];

  tableRows.forEach(row => {
    row.forEach((cell, i) => {
      const valueLength = getCellValue(cell).length;
      result[i] = Math.max(result[i] || 0, valueLength);
    });
  });

  return result.map(numberOfSymbols =>
    Math.max(MIN_ROW_WIDTH, numberOfSymbols * SYMBOL_LENGTH + PADDING)
  );
}

function createTableCell({ left, top, width, height }, cell) {
  const fillColor = cell?.background
    ? new PSPDFKit.Color({ r: 217, g: 217, b: 217 })
    : new PSPDFKit.Color({ r: 255, g: 255, b: 255 });

  return new RectangleAnnotation({
    pageIndex: 0,
    boundingBox: new Rect({
      left: left - STROKE_WIDTH / 2,
      top: top - STROKE_WIDTH / 2,
      width: width + STROKE_WIDTH,
      height: height + STROKE_WIDTH
    }),
    strokeColor: new PSPDFKit.Color({ r: 184, g: 184, b: 184 }),
    strokeWidth: STROKE_WIDTH,
    fillColor,
    customData: { disableEditing: true, tableCell: true, order: 2 },
    createdAt: new Date(),
    updatedAt: new Date()
  });
}

function createCellText({ left, top, width, height }, cell) {
  return new TextAnnotation({
    pageIndex: 0,
    text: { format: 'plain', value: getCellValue(cell) },
    font: 'Courier',
    fontSize: FONT_SIZE,
    verticalAlign: 'center',
    horizontalAlign: 'left',
    boundingBox: new Rect({ left: left + PADDING, top, width, height }),
    fontColor: PSPDFKit.Color.BLACK,
    customData: { disableEditing: true, tableCell: true, order: 3 },
    createdAt: new Date(),
    updatedAt: new Date()
  });
}

/**
 * @param {Array<Array>} tableRows
 * @param {{width: Number, height: Number}} pdfSizes
 */
async function createTableAnnotations(tableRows, pdfSizes) {
  const columnsLengthList = calculateColumnsLength(tableRows);
  const tableHeight = tableRows.length * ROW_HEIGHT;
  const tableWidth = columnsLengthList.reduce((res, length) => res + length, 0);

  const startXPosition = pdfSizes.width - tableWidth - DRAWING_BORDER_WIDTH;
  let currentXPosition = startXPosition;
  let currentYPosition = pdfSizes.height - tableHeight - BOTTOM_POSITION;

  const cells = [];
  const texts = [];

  tableRows.forEach(row => {
    currentXPosition = startXPosition;

    columnsLengthList.forEach((width, columnIndex) => {
      const position = {
        left: currentXPosition,
        top: currentYPosition,
        width,
        height: ROW_HEIGHT
      };

      cells.push(createTableCell(position, row[columnIndex]));

      if (getCellValue(row[columnIndex])) {
        texts.push(createCellText(position, row[columnIndex]));
      }

      currentXPosition = currentXPosition + width;
    });

    currentYPosition = ROW_HEIGHT + currentYPosition;
  });

  return { cells, texts };
}

/**
 * @param {Instance} instance
 */
async function removeTableAnnotations(instance) {
  const annotations = await instance.getAnnotations(0);

  const tableCellIds = [];
  annotations.forEach(annotation => {
    if (annotation?.customData?.tableCell) tableCellIds.push(annotation.id);
  });

  if (tableCellIds.length) return instance.delete(tableCellIds);
}

/**
 * @param {Instance} instance
 */
async function saveAnnotationsInPdf(instance) {
  await instance.save();
  const arrayBuffer = await instance.exportPDF();

  return new Blob([arrayBuffer], { type: 'application/pdf' });
  // !MANUAL SORTING OF ANNOTATIONS
  // const XFDF = await instance.exportXFDF();
  // const sortedXFDF = sortAnnotations(XFDF);

  // const config = {
  //   baseUrl: `${window.location.protocol}//${window.location.host}/assets/pspdfkit/`,
  //   document: arrayBuffer,
  //   XFDF: sortedXFDF,
  //   headless: true
  // };

  // if (LICENSE_KEY) config.licenseKey = LICENSE_KEY;

  // let tempInstance = await PSPDFKit.load(config);

  // return tempInstance.exportPDF().then(pdfArrayBuffer => {
  //   unloadPspdfkit(tempInstance);
  //   return new Blob([pdfArrayBuffer], { type: 'application/pdf' });
  // });
}

/**
 * @param {String} XFDF
 */
function sortAnnotations(XFDF) {
  const parser = new DOMParser();
  let xmlDoc = parser.parseFromString(XFDF, 'text/xml');
  let annotationsArray = Array.from(
    xmlDoc.childNodes[0].childNodes[0].childNodes
  );

  const getCustomDataString = node => {
    if (!node.childNodes?.length) return '';

    let result = '';

    node.childNodes.forEach(node => {
      if (result) return;

      if (node.nodeName === 'pspdf-custom-data') {
        result = node.firstChild.nodeValue;
      } else {
        const res = getCustomDataString(node);
        if (res) result = res;
      }
    });

    return result;
  };

  annotationsArray.sort((a, b) => {
    const aCustomData =
      a.nodeName === '#text' ? '{"order": 3}' : getCustomDataString(a);
    const bCustomData =
      b.nodeName === '#text' ? '{"order": 3}' : getCustomDataString(b);
    let aOrder = '{"order": 0}';
    let bOrder = '{"order": 0}';

    try {
      aOrder = JSON.parse(aCustomData).order;
    } catch (error) {
      console.log('annotationsArray.sort ~ error:', error);
    }

    try {
      bOrder = JSON.parse(bCustomData).order;
    } catch (error) {
      console.log('annotationsArray.sort ~ error:', error);
    }

    if (aOrder > bOrder) return 1;
    if (aOrder < bOrder) return -1;
    return 0;
  });

  annotationsArray.forEach(node => {
    xmlDoc.childNodes[0].childNodes[0].appendChild(node);
  });

  const serializer = new XMLSerializer();
  const sortedXFDF = serializer.serializeToString(xmlDoc);

  annotationsArray = null;
  xmlDoc = null;

  return sortedXFDF;
}

/**
 *
 * @param {Instance} pspdfkitInstance
 * @param {Array<Array>} tableRows
 */
async function drawTable(pspdfkitInstance, tableRows) {
  if (!pspdfkitInstance) {
    console.error('PSPDFKIT is not initialized');
    return;
  }

  await removeTableAnnotations(pspdfkitInstance);

  const { cells, texts } = await createTableAnnotations(
    tableRows,
    pspdfkitInstance.pageInfoForIndex(0)
  );

  // create annotations separately to make sure that they will be in the right order
  // ! doesn't work in pspdfkit for now
  await pspdfkitInstance.create(cells);

  await pspdfkitInstance.create(texts);
}

/**
 * @param {string=} selector if empty will run pspdfkit in headless mode
 */
export function usePspdfkit(selector) {
  let pspdfkitInstance;

  return {
    async initViewer(pdfPath) {
      unloadPspdfkit(pspdfkitInstance);
      pspdfkitInstance = await loadPspdfkit(selector, pdfPath);
    },
    destroyViewer() {
      unloadPspdfkit(pspdfkitInstance);
    },
    drawTable(tableRows) {
      return drawTable(pspdfkitInstance, tableRows);
    },
    saveAnnotationsInPdf() {
      return saveAnnotationsInPdf(pspdfkitInstance);
    }
  };
}

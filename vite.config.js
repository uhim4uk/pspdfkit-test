import { defineConfig } from 'vite';
// import vue from '@vitejs/plugin-vue';


export default defineConfig({
  // plugins: [vue(), viteCompression(compressionConfig)],

  server: {
    host: '0.0.0.0',
    port: 8080
  },

  build: {
    reportCompressedSize: false
  },

  // test: {
  //   // enable jest-like global test APIs
  //   globals: true,
  //   // simulate DOM with happy-dom
  //   // environment: 'happy-dom',
  //   environment: 'jsdom',
  //   setupFiles: ['./tests/unit/setup.js']
  // }
});

import {usePspdfkit} from './usePspdfkit.js';

const tableRows = [
  [
    "Section",
    "X",
    "Y",
    "Z",
    "Input",
    "Rotation",
    "Angle",
    "Overlength",
    "Length",
    "Radius"
  ],
  [
    {
      "background": true,
      "value": "#01"
    },
    {
      "background": true
    },
    {
      "background": true
    },
    {
      "background": true
    },
    {
      "background": true
    },
    {
      "background": true
    },
    {
      "background": true
    },
    {
      "background": true,
      "value": 0
    },
    {
      "background": true,
      "value": 3495
    },
    {
      "background": true,
      "value": 285
    }
  ],
  [1, -1074, 1074, 0, 1401, 0, 0, 0],
  [2, -1988, 0, 0, 1870, 0, 45, 0]
];

const saveButton = document.getElementById('save-button');
const addButton = document.getElementById('add-annotations-button');

const {initViewer, destroyViewer, drawTable, saveAnnotationsInPdf} = usePspdfkit('#pspdfkit-wrapper');

function downloadBlob(blob) {
  const dlink = document.createElement('a');
  dlink.href = window.URL.createObjectURL(blob);
  dlink.onclick = function (e) {
    setTimeout(function () {
      window.URL.revokeObjectURL(e.target.href);
    }, 1500);
  };

  dlink.setAttribute('download', 'testAnnotations');
  document.body.appendChild(dlink);

  dlink.click();
  dlink.remove();
}

initViewer('/assets/test.pdf').then(() => {
  saveButton.addEventListener('click', () => {
    saveAnnotationsInPdf().then(downloadBlob);
  })
  
  addButton.addEventListener('click', () => {
    drawTable(tableRows);
  })
})
